#include <Servo.h>

Servo ser;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  ser.attach(2);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()>0)
  {
    int pos=Serial.parseInt();
    Serial.println(pos);
    ser.write(pos);
  }
}
