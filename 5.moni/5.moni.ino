void setup() {
  // put your setup code here, to run once:
  pinMode(10,OUTPUT);
  pinMode(2,INPUT_PULLUP);
  pinMode(12,INPUT_PULLUP);
}
int n=0;
void loop() {
  // put your main code here, to run repeatedly:
  int button1=digitalRead(2);
  int button2=digitalRead(12);
  if(button1 == LOW && n<255)
  {
    n++;
  }
  if(button2 == LOW && n>0)
  {
    n--;
  }
  delay(100);
  analogWrite(11,n);
}
