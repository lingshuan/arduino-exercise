bool number[10][7]={
  {1,1,1,1,1,1,0},
  {0,1,1,0,0,0,0},
  {1,1,0,1,1,0,1},
  {1,1,1,1,0,0,1},
  {0,1,1,0,0,1,1},
  {1,0,1,1,0,1,1},
  {1,0,1,1,1,1,1},
  {1,1,1,0,0,0,0},
  {1,1,1,1,1,1,1},
  {1,1,1,1,0,1,1}};
char w[7]={3,4,7,8,9,6,5};
bool flag=false;
char n=0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(2,INPUT_PULLUP);
  pinMode(13,OUTPUT);
  
  for(int i=3;i<10;i++)
    pinMode(i,OUTPUT);
  randomSeed(analogRead(A0));
}

void loop() {
  // put your main code here, to run repeatedly:
  int button=digitalRead(2);
  Serial.println(button);
  if(button==LOW && flag==false)
  {
    n=random(10);
    flag=true;
  }
  if(button==HIGH && flag==true)
  {
    flag=false;
  }
  for(int i=0;i<7;i++)
  {
    digitalWrite(w[i],number[n][i]);
  }
}
