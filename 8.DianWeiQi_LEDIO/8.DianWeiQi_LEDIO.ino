void setup() {
  // put your setup code here, to run once:
  pinMode(9,OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int i=analogRead(A0);
  int j=map(i,0,1023,0,255);
  Serial.println(j);
  analogWrite(9,j);
}
