#include <Servo.h>

int v=0;
Servo ser;
void setup() {
  // put your setup code here, to run once:
  pinMode(9,OUTPUT);
  pinMode(2,INPUT_PULLUP);
  ser.attach(9);
}
void an()
{
  int button=digitalRead(2);
  if(LOW==button)
  {
    v=!v;
    while(!button)
    {
      button=digitalRead(2);
    }
  }
}
void loop() 
{
  for(int i=45;i<135;i+=v)
  {
    ser.write(i);
    an();
    delay(10);
  }
  for(int i=135;i>45;i-=v)
  {
    ser.write(i);
    an();
    delay(10);
  }
}
