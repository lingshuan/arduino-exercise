bool number[10][7]={
  {1,1,1,1,1,1,0},
  {0,1,1,0,0,0,0},
  {1,1,0,1,1,0,1},
  {1,1,1,1,0,0,1},
  {0,1,1,0,0,1,1},
  {1,0,1,1,0,1,1},
  {1,0,1,1,1,1,1},
  {1,1,1,0,0,0,0},
  {1,1,1,1,1,1,1},
  {1,1,1,1,0,1,1}};
bool f[7]={1,0,0,0,1,1,1};
char w[7]={3,4,7,8,9,6,5};
char n=0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(2,INPUT);
  pinMode(13,OUTPUT);
  
  for(int i=3;i<10;i++)
    pinMode(i,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int button=digitalRead(2);
  Serial.println(button);
  while(button==LOW)
  {
    button=digitalRead(2);
    for(int i=0;i<7;i++)
    {
      digitalWrite(w[i],f[i]);
    }
  }
  for(int i=0;i<7;i++)
  {
    digitalWrite(w[i],number[n][i]);
  }
  n=(n+1)%10;
  delay(500);
}
